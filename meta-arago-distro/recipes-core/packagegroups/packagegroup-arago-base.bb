DESCRIPTION = "Basic task to get a device booting"
LICENSE = "MIT"
PR = "r9"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

ARAGO_ALSA_BASE = "\
    libasound \
    alsa-utils-aplay \
    "

ARAGO_BASE = "\
    module-init-tools \
    mtd-utils \
    mtd-utils-ubifs \
    initscript-telnetd \
    thermal-init \
    udev-extraconf \
    cmake stat gdb rsync tar tmux readline grep lrzsz htop less mc man \
    ninja findutils strace lsof dtc sed util-linux openssh-sftp-server \
    nano time tzdata tzdata-europe tzdata-americas tzdata-posix wget \
    iputils-ping iputils-ping6 bash-completion bash-completion-extra \
    systemd-extra-utils systemd-bash-completion dhcp-client fish curl \
    qtquickcontrols-qmlplugins qttools-plugins qt5-creator iotop jq ell \
    ctags cscope dash vim opencv tree zbar binutils cogl-1.0 git bash \
    diffutils elfutils file i2c-tools lsof lz4 m4 meson minicom mktemp \
    packagegroup-core-ssh-openssh patchelf patch procps quilt socat xz \
    zip unzip iproute2 man-pages openssh-scp openssh-ssh openssh-sftp \
    openssh-sshd openssh-keygen ethtool fstl \
    "

# these require meta-openembedded/meta-oe layer
ARAGO_EXTRA = "\
    devmem2 \
    tcpdump \
    "

# minimal set of packages - needed to boot
RDEPENDS_${PN} = "\
    ${@bb.utils.contains('MACHINE_FEATURES', 'alsa', '${ARAGO_ALSA_BASE}', '',d)} \
    ${ARAGO_BASE} \
    ${ARAGO_EXTRA} \
    "
